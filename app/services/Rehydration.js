import ReduxPersist from '../config/ReduxPersist'
import { AsyncStorage } from 'react-native'
import { persistStore } from 'redux-persist'
import StartupActions from '../redux/StartupRedux'

const REDUCER_VERSION = 'reducerVersion'

const updateReducers = (store: Object) => {
  const reducerVersion = ReduxPersist.reducerVersion
  const startup = () => store.dispatch(StartupActions.startup())

  AsyncStorage.getItem(REDUCER_VERSION)
    .then(localVersion => {
      if (localVersion !== reducerVersion) {
        persistStore(store, null, startup).purge()
        AsyncStorage.setItem(REDUCER_VERSION, reducerVersion)
      } else {
        persistStore(store, null, startup)
      }
    })
    .catch(() => {
      persistStore(store, null, startup)
      AsyncStorage.setItem(REDUCER_VERSION, reducerVersion)
    })
}

export default { updateReducers }