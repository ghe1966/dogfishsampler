import API from '../services/Api'

const api = API.create()

export default (requestStr) => {
  return api.apiGetRequest(requestStr)
}