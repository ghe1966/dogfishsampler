// a library to wrap and simplify api calls
import apisauce from 'apisauce'
import AppConfig from '../config/AppConfig'


const create = (baseURL = AppConfig.resortsURL) => {
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 10000
  })

  // TODO: define  all your API calls here (GET, POST,DELETE etc...)
  const apiGetRequest = endpoint => api.get(endpoint)

  return {
    apiGetRequest
  }
}

// let's return back our create method as the default.
export default {
  create
}