import R from 'ramda'
import Immutable from 'seamless-immutable'

const immutable = Immutable

const isImmutable = R.has('asMutable')

// converting to mutable - deep copy making it mutable

const convertToJs = state => state.asMutable({ deep: true })

const fromImmutable = R.when(isImmutable, convertToJs)

const toImmutable = raw => immutable(raw)

export default {
  out: state => toImmutable(state),
  in: raw => fromImmutable(raw)
}