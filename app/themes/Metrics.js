export default {
    defaultBorderRadius: 15,
    defaultMargin: 15,
    cardHeight: 230,
    smallMargin: 10,
    tabIconSize: 21
}
