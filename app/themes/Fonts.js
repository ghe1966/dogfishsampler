import React from 'react'
import { Platform } from 'react-native'

const type = {
    ...Platform.select({
        ios: {
            base: 'Avenir-Book',
            bold: 'Avenir-Black',
            emphasis: 'HelveticaNeue-Italic',
            facebook: 'FacebookLetterFaces',
            androidLogo: 'hobo_std'
        },
        android: {
            base: 'Roboto',
            bold: 'Roboto-Black',
            emphasis: 'Roboto-Thin',
            facebook: 'FacebookLetterFaces',
            androidLogo: 'hobo_std'
        }
    })
}

const size = {
    h1: 38,
    h2: 34,
    h3: 30,
    h4: 26,
    h5: 21,
    h6: 19,
    input: 18,
    regular: 17,
    medium: 14,
    small: 12,
    tiny: 8.5
}

const style = {
    title: {
        fontFamily: type.base,
        fontSize: size.h5
    },
    description: {
        fontFamily: type.emphasis,
        fontSize: size.regular
    }
}

export default {
    type,
    size,
    style
}
