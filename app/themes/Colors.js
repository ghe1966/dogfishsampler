import ColorsPalette from './ColorsPalette'

export default {
    secondary: ColorsPalette.skyBlue,
    secondaryLight: ColorsPalette.skyBlueLight,
    secondaryDark: ColorsPalette.skyBlueDark,
    textOnPrimary: ColorsPalette.white,
    textOnSecondary: ColorsPalette.black,
    white: ColorsPalette.white
}
