export default {
    skyBlue: '#5ff0ef',
    skyBlueLight: '#99ffff',
    skyBlueDark: '#00bdbd',
    white: '#ffffff',
    black: '#000000'
}
