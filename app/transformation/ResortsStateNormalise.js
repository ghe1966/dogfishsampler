export const entriesStateNormalise = (entries) => {
  const initialState = {byId:{},allIds:[]}
  if(entries) {
    return entries.reduce((previous, current) => {
      return {
        byId: {...previous.byId, [current._id]: current},
        allIds: [...previous.allIds, current._id]
      }
    }, initialState)
  }
  return initialState
}

