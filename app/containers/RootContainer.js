import React, { Component } from 'react'
import { View} from 'react-native'
import { connect } from 'react-redux'
import ReduxPersist from '../config/ReduxPersist'
import StartupActions from '../redux/StartupRedux'
import ResortActions from '../redux/ResortRedux'
import AppNavigation from '../navigation/AppNavigation'

class RootContainer extends Component {
  componentDidMount() {
    if (!ReduxPersist.active) {
      this.props.startup()
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.start.active !== prevProps.start.active) {
      this.props.getCountriesResorts()
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
       <AppNavigation/>
      </View>
    )
  }
}

const mapStateToProps = state => ({

    start: state.startup
})

const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup()) ,
  getCountriesResorts: () => dispatch(ResortActions.getCountriesResorts())

})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RootContainer)