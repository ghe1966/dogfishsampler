import React, { Component } from 'react'
import {ScrollView, View} from 'react-native'
import { Card, Text} from 'react-native-elements'
import List from '../components/List'
import {connect} from 'react-redux'
import AppConfig from '../config/AppConfig'
import styles from './styles/ResortDetailStyles'

class ResortDetailScreen extends Component {

  static navigationOptions = ({navigation, navigationOptions}) => {
    const {params} = navigation.state
    return {
      title: `${params.title} Details`
    }
  }

  render() {

    const {navigation} = this.props
    const resortObj = navigation.getParam('resort', null)

    if (this.props.error && this.props.error.errorCode) {
      return <View><Text>{this.props.error.errorMessage}</Text></View>
    }

    return (
      <ScrollView>
        <Card
          image={{uri: AppConfig.imageURL + resortObj.image}}
          imageStyle={styles.imageStyle}>
          <Text h4>{resortObj.name} Hotels</Text>
          <List
            items={this.props.resortDetail}
          />
        </Card>
      </ScrollView>
    )
  }
}
const mapStateToProps = state => {
  return {
    resortDetail: state.resortDetail.resortDetail,
    error: state.resortDetail.error
  }
}
export default connect(mapStateToProps, null)(ResortDetailScreen)