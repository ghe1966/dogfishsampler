
import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {connect} from 'react-redux'
import List from '../components/List'

class HomeScreen extends Component {

  onPress = (id) => {
    const country = this.props.countries.byId[id].name
    this.props.navigation.navigate('Resorts', {title: country})
  }

  render() {
    if (this.props.error && this.props.error.errorCode) {
      return <View><Text>{this.props.error.errorMessage}</Text></View>
    }
    return (
      <View>
       <List
         items={this.props.countries}
          onPress={this.onPress}
       />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    countries : state.resorts.countries,
    error: state.resorts.error
  }
}

export default connect(mapStateToProps, null)(HomeScreen)