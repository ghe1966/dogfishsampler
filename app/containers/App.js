import '../config'
import React from 'react'
import { YellowBox } from 'react-native'
import { Provider } from 'react-redux'
import RootContainer from './RootContainer'
import createStore from '../redux'

const store = createStore()

YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: isMounted(...) is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
  'Module RCTImageLoader requires',
  'Class RCTCxxModule'
])
export default () => {
  return (
    <Provider store={store}>
      <RootContainer />
    </Provider>
  )
}
