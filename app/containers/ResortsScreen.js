import React, {Component} from 'react'
import {View} from 'react-native'
import {connect} from 'react-redux'
import List from '../components/List'
import {entriesStateNormalise} from '../transformation'
import ResortDetailActions from '../redux/ResortDetailRedux'

class ResortsScreen extends Component {

  static navigationOptions = ({navigation, navigationOptions}) => {
    const {params} = navigation.state
    return {
      title: `Resorts in ${params.title}`
    }
  }

  filterResorts = () => {
    const {navigation, resorts} = this.props
    const countryName = navigation.getParam('title', null)
    return Object.values(resorts.byId)
      .filter(resort => resort.country === countryName)
  }

  onPress = (id) => {
    this.props.getResortDetail(id)
    const resortName = this.props.resorts.byId[id].name
    this.props.navigation.navigate('ResortDetail', {title: resortName, resort: this.props.resorts.byId[id]})
  }

  render() {
    return (
      <View>
        <List
          items={entriesStateNormalise(this.filterResorts())}
          onPress={this.onPress}
        />
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    resorts: state.resorts.resorts
  }
}
const mapDispatchToProps = dispatch => ({

  getResortDetail: (itemId) => dispatch(ResortDetailActions.getResortDetail(itemId))

})

export default connect(mapStateToProps, mapDispatchToProps)(ResortsScreen)