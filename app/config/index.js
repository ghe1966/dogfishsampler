import { Text } from 'react-native'
import AppConfig from './AppConfig'

if (Text.defaultProps == null) Text.defaultProps = {};
Text.defaultProps.allowFontScaling = AppConfig.allowTextFontScaling