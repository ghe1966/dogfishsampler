export default {
  allowTextFontScaling: true,
  resortsURL: 'https://crystal-dev.ski-day.com/live/',
  imageURL:'https://crystal-dev.ski-day.com/public/images/',
}
