import { takeLatest, all } from 'redux-saga/effects'

/* ------------- Types ------------- */

import { StartupTypes } from '../redux/StartupRedux'
import { ResortTypes } from '../redux/ResortRedux'
import { ResortDetailTypes } from '../redux/ResortDetailRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getCountriesResorts } from './ResortSagas'
import { getResortDetail } from './ResortDetailSagas'

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(ResortTypes.GET_COUNTRIES_RESORTS, getCountriesResorts),
    takeLatest(ResortDetailTypes.GET_RESORT_DETAIL, getResortDetail),

  ])
}