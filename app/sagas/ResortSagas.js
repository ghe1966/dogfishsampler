
import ResortActions from '../redux/ResortRedux'
import { put } from 'redux-saga/effects'
import ApiService from '../services/ApiServices'

export function* getCountriesResorts() {

  try{
    const responseData = yield ApiService('resorts.json')
    yield put(ResortActions.saveCountries(responseData.data.countries))
    yield put(ResortActions.saveResorts(responseData.data.resorts))

  } catch(e) {
    const error = {
        errorCode: 400,
        errorMessage: 'Error accessing server'
    }
    yield put(ResortActions.apiError(error))
  }

}