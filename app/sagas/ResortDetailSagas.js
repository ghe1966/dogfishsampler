
import ResortDetailActions from '../redux/ResortDetailRedux'
import { put } from 'redux-saga/effects'
import ApiService from '../services/ApiServices'

export function* getResortDetail(action) {

  try{
    const requestStr = 'resorts/' + action.itemId + '.json'
    const responseData = yield ApiService(requestStr)
    yield put(ResortDetailActions.saveResortDetail(responseData.data.hotels))

  } catch(e) {
    const error = {
        errorCode: 400,
        errorMessage: 'Error Accessing Resort Details'
    }
    yield put(ResortDetailActions.apiDetailError(error))
  }
}