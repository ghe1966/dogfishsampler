import React from 'react'
import { FlatList } from 'react-native'
import { ListItem } from 'react-native-elements'

export default ({items, onPress}) => {

    const keyExtractor = item => {
      return item
    }

  const renderListItem = item => {
    return (
      <ListItem
        key={item._id}
        title={item.name}
       onPress={() => onPress? onPress(item._id) : null}/>
    )
  }
  return (
    <FlatList
      data={ items.allIds }
      renderItem={({ item }) => renderListItem( items.byId[item] )}
      keyExtractor={keyExtractor}
    />
  )
}


