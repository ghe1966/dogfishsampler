import { createStore, applyMiddleware, compose } from 'redux'
import Logger from 'redux-logger'
import Rehydration from '../services/Rehydration'
import ReduxPersist from '../config/ReduxPersist'
import createSagaMiddleware from 'redux-saga'

export default (rootReducer, rootSaga) => {
  const middleware = []
  const enhancers = []

  const sagaMiddleware = createSagaMiddleware({})
  middleware.push(sagaMiddleware)
  middleware.push(Logger)

  enhancers.push(applyMiddleware(...middleware))

  const store = createStore(rootReducer, compose(...enhancers))

  if (ReduxPersist.active) {
    Rehydration.updateReducers(store)
  }

  let sagasManager = sagaMiddleware.run(rootSaga)

  return {
    store,
    sagasManager,
    sagaMiddleware
  }
}
