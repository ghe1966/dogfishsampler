import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import configureStore from './CreateStore'
import rootSaga from '../sagas'
import ReduxPersist from '../config/ReduxPersist'

export const reducers = combineReducers({
  startup: require('./StartupRedux').reducer,
  resorts: require('./ResortRedux').reducer,
  resortDetail: require('./ResortDetailRedux').reducer,
})

export default () => {
  let finalReducers = reducers
  if (ReduxPersist.active) {
    const persistConfig = ReduxPersist.storeConfig
    finalReducers = persistReducer(persistConfig, reducers)
  }

  let { store, sagasManager, sagaMiddleware } = configureStore(finalReducers, rootSaga)

  // part of redux saga - webpack config - hot module replacement so if
  // anything changes in the saga functions they are re-run
  // https://webpack.js.org/api/hot-module-replacement/

  if (module.hot) {
    module.hot.accept(() => {
      store.replaceReducer(require('./').reducers)
      const newYieldedSagas = require('../sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}