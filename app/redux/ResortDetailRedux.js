

import { createActions, createReducer } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { entriesStateNormalise } from '../transformation'

/* ------------- Types and Action Creators ------------- */

const immutable = Immutable

//action
const { Types, Creators } = createActions({
  getResortDetail: ['itemId'],
  saveResortDetail: ['resortDetail'],
  apiDetailError: ['error']
})


//types
export const ResortDetailTypes = Types
export default Creators


export const INITIAL_STATE = immutable({
  resortDetail: {
    allIds: [],
    byId: {}
  },
  error: {
    errorCode: null,
    errorMessage: ''
  },
})

//reducer
export const saveResortDetail = (state, {resortDetail}) =>
  ({
    ...state,
    resortDetail: entriesStateNormalise(resortDetail),
    error: INITIAL_STATE.error,
  })
export const apiDetailError = (state, {error}) =>
  ({
    ...INITIAL_STATE,
    error : error,
  })


export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_RESORT_DETAIL]: null,
  [Types.SAVE_RESORT_DETAIL]: saveResortDetail,
  [Types.API_DETAIL_ERROR]: apiDetailError,

})