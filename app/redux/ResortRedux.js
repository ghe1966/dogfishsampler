
import { createActions, createReducer } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { entriesStateNormalise } from '../transformation'

/* ------------- Types and Action Creators ------------- */

const immutable = Immutable

//action
const { Types, Creators } = createActions({
  getCountriesResorts: null,
  saveCountries: ['countries'],
  saveResorts: ['resorts'],
  apiError: ['error']
})


//types
export const ResortTypes = Types
export default Creators


export const INITIAL_STATE = immutable({
  countries: {
    allIds: [],
    byId: {}
  },
  resorts: {
    allIds: [],
    byId: {}
  },
  error: {
    errorCode: null,
    errorMessage: ''
  }
})

//reducer
export const saveCountries = (state, {countries}) =>
  ({
    ...state,
    countries: entriesStateNormalise(countries),
    error: INITIAL_STATE.error,
  })

export const saveResorts = (state, {resorts}) =>
  ({
    ...state,
    resorts : entriesStateNormalise(resorts),
    error: INITIAL_STATE.error,


  })
export const apiError = (state, {error}) =>
  ({
    ...INITIAL_STATE,
    error : error,


  })


export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_COUNTRIES_RESORTS]: null,
  [Types.SAVE_COUNTRIES]:  saveCountries,
  [Types.SAVE_RESORTS]:  saveResorts,
  [Types.API_ERROR]:  apiError,

})