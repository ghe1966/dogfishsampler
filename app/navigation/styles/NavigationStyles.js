import { StyleSheet } from 'react-native'
import { Colors } from '../../themes/'

export default StyleSheet.create({
    header: { backgroundColor: Colors.secondaryDark },
    headerTitle: {
        color: Colors.white,
        height: 20,
        fontSize: 18
    }
})
