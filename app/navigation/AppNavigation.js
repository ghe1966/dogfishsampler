import React from 'react'
import {StackNavigator} from 'react-navigation'


import HomeScreen from '../containers/HomeScreen'
import ResortsScreen from '../containers/ResortsScreen'
import ResortDetailScreen from '../containers/ResortDetailScreen'
import {Colors, Metrics} from '../themes'
import styles from './styles/NavigationStyles'

const primaryNav = StackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: () => ({title: 'Resort Countries'})
    },
    Resorts: {
      screen: ResortsScreen
    },
    ResortDetail: {
      screen: ResortDetailScreen,
    }
  },
  {
    navigationOptions: () => ({
      initialRouteName: 'ResortsScreen',
      headerStyle: styles.header,
      headerMode: 'screen',
      headerTintColor: Colors.white,
      headerTitleStyle: styles.headerTitle
    })
  }
)

export default primaryNav
