/** @format */

import {AppRegistry} from 'react-native';
import App from './app/containers/App';

AppRegistry.registerComponent("DogfishSampler", () => App);
